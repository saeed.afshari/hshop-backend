# hshop
HSE24 shop application.
You can get access to the project repository [here](https://gitlab.com/saeed.afshari/hshop-backend).   

## Requirements

The application has been developed with Java 11, so to be able to launch the application you need to install the following dependencies:
- [OpenJdk 11]
You can download the OpenJdk using the following commands:
```
apt-get update
apt-get -y install openjdk-11-jdk
```

- Docker (If you want to install MySQL container and test under prod profile)
- docker-compose (If you want to install MySQL container and test under prod profile)

## How to build the application and run the tests
In order to build the application run the following command:
```
./gradlew clean build
```
After successful build, the executable jar file will be created under ./build/lib directory.

In order to run unit tests use the following command:
```
./gradlew test
```

## How to start the application

You can start application under two different environment configurations:
- local
- prod  

### local
To run application quickly without any need to ramp up any database server use the following commands:
1. If the jar file available under ./build/lib:
```
java -jar -Dspring.profiles.active=local build/libs/hshop-0.0.1.jar
```
2. use gradle command
```
./gradlew bootRun -Dspring.profiles.active=local
```

The above command will run the application through the following steps:
- H2 As an in-memory database will be loaded
- Liquibase will create necessary tables and insert sample records into the database (init scripts are available under /resources/db/changelog)
- The application will be started on port 8080

### prod
To run application and test application with a production ready database server such as MySQL Server, first the database server should be become up and running.
To do so, the docker-compose file under ./docker/mysql-db/docker-compose.yml path created.
After installing docker and docker-compose by executing the following command the database container will be created.
```
docker-compose -f docker/mysql-db/docker-compose.yml up -d
```
The above command will ramp up MySQL server container, listening on 4321 port number.
It also creates a database named "hshop" which will be used as the project database.

When the database is ready, in order to run the application the following commands can be used:
1. If the jar file available under ./build/lib:
```
java -jar -Dspring.profiles.active=prod build/libs/hshop-0.0.1.jar
```
2. use gradle command
```
./gradlew bootRun -Dspring.profiles.active=prod
```

The above command will run the application through the following steps:
- Liquibase will create necessary tables and insert sample records into the hshop database inside MySQL container (init scripts are available under /resources/db/changelog)
- The application will be started on port 8080  

## API Health check
The application is using Actuator library to provide health check mechanism.
After launching the application, you can use the following url to check the API healthiness: 

```
http://localhost:8080/actuator/health
```

## API Documentation
To document the developed API end-points swagger2 is being used.
You can get access the API documentation via the following end-point:
```
http://localhost:8080/v2/api-docs
```

### API visualisation and interaction 
To visualise the API documentation you can copy the json object generated out of visiting the above link and paste it in
[Swagger Editor](https://editor.swagger.io/) to be able to interact with available APIs.

Or you can also see the API documentation using the following link:
```
http://localhost:8080/swagger-ui.html
```
