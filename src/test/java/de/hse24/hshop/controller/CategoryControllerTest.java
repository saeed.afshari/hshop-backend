package de.hse24.hshop.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.hse24.hshop.controller.product.CategoryController;
import de.hse24.hshop.controller.product.dto.CategoryCreateOrUpdateDto;
import de.hse24.hshop.controller.product.dto.CategoryResponseDto;
import de.hse24.hshop.controller.product.dto.mapper.CategoryMapper;
import de.hse24.hshop.controller.product.dto.mapper.ProductMapper;
import de.hse24.hshop.product.model.Category;
import de.hse24.hshop.product.service.CategoryService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * API tests for CategoryController
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = {CategoryController.class})
public class CategoryControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CategoryService categoryService;

    @MockBean
    private CategoryMapper categoryMapper;

    @MockBean
    private ProductMapper productMapper;

    private static ObjectMapper objectMapper;

    private Category category;

    private static final String apiBaseUrl = "/v1/category";

    @BeforeAll
    static void beforeAll() {
        objectMapper = new ObjectMapper();
    }

    @BeforeEach
    void beforeEach() {
        category = new Category();
        category.setName("Fashion");
        category.setLevel(1);
    }

    @Test
    void validInputProvidedViaGetCallThenReturns200() throws Exception {
        mockMvc.perform(get(apiBaseUrl + "/1")
                .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    void validInputProvidedToGetAllWithNameAPIThenShouldReturns200() throws Exception {
        mockMvc.perform(get(apiBaseUrl + "/name/aName/level/1")
                .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    void validInputProvidedToGetAllWithLevelAPIThenShouldReturns200() throws Exception {
        mockMvc.perform(get(apiBaseUrl + "/level/1")
                .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    void validInputProvidedToCreateAPIThenShouldReturns201() throws Exception {
        when(categoryMapper.toCategory(any(CategoryCreateOrUpdateDto.class))).thenReturn(category);

        mockMvc.perform(MockMvcRequestBuilders
                .post(apiBaseUrl)
                .content(asJsonString(category))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void validInputProvidedToUpdateAPIThenShouldReturns200() throws Exception {
        when(categoryMapper.toCategory(any(CategoryCreateOrUpdateDto.class))).thenReturn(category);

        mockMvc.perform(MockMvcRequestBuilders
                .put(apiBaseUrl + "/1")
                .content(asJsonString(category))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    public String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
