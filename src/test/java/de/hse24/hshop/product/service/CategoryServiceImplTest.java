package de.hse24.hshop.product.service;

import de.hse24.hshop.common.exception.DuplicatedEntityFoundException;
import de.hse24.hshop.common.exception.EntityNotFoundException;
import de.hse24.hshop.product.exception.InvalidEntityException;
import de.hse24.hshop.product.model.Category;
import de.hse24.hshop.product.repository.CategoryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class CategoryServiceImplTest {

    private CategoryServiceImpl categoryService;

    private final Category level2Category = new Category();
    private final Category level1Category = new Category();

    @Mock
    private CategoryRepository categoryRepository;

    @BeforeEach
    void beforeEach() {
        categoryService = new CategoryServiceImpl(categoryRepository);

        level1Category.setLevel(1);
        level1Category.setName("Fashion");

        level2Category.setLevel(2);
        level2Category.setName("Fashion2");
    }

    @Test
    void categoryWithTheSameNameinSameLevelExistsThenWhouldThrowDuplicatedEntityFoundException() {
        when(categoryRepository.findByNameAndLevel(any(String.class), any(Integer.class)))
                .thenReturn(Collections.singletonList(level1Category));

        Assertions.assertThrows(DuplicatedEntityFoundException.class, () -> categoryService.save(level1Category));
    }

    @Test
    void categoryLevelIsMoreThanOneAndParentIsNullExistsThenWhouldThrowInvalidEntityException() {
        Assertions.assertThrows(InvalidEntityException.class, () -> categoryService.save(level2Category));
    }

    @Test
    void categoryLevelIsMoreThanOneAndParentIsInvalidThenWhouldThrowEntityNotFoundException() {
        Assertions.assertThrows(InvalidEntityException.class, () -> categoryService.save(level2Category));
    }

    @Test
    void categoryLevelIsMoreThanOneThenTheLinkShouldBeCreated() {
        when(categoryRepository.save(level1Category))
                .thenReturn(level1Category);
        Assertions.assertEquals(level1Category.getName(), categoryService.save(level1Category).getLink());
    }

    @Test
    void categoryLevelIsMoreThanOneAndParentCategoryProvidedThenTheLinkShouldBeCreated() {
        level2Category.setParentCategory(level1Category);

        when(categoryRepository.save(level2Category))
                .thenReturn(level2Category);
        when(categoryRepository.findById(level2Category.getParentCategory().getId()))
                .thenReturn(Optional.of(level1Category));

        Assertions.assertEquals(level1Category.getName() + "/" + level2Category.getName(),
                categoryService.save(level2Category).getLink());
    }

    @Test
    void categoryIsNotfoundThenThrowEntityNotFoundException() {
        when(categoryRepository.findById(level1Category.getId()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(EntityNotFoundException.class, () -> categoryService.update(level1Category));
    }

    @Test
    void generateLinkShouldGenerateAValidLink() {
        level2Category.setParentCategory(level1Category);
        Assertions.assertEquals(String.format("%s/%s", level1Category.getName(), level2Category.getName()),
                categoryService.generateLink(level2Category));
    }

}
