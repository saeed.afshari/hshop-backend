package de.hse24.hshop.product.exception;

public class HasSubCategoryException extends RuntimeException {
    public HasSubCategoryException(String message) {
        super(message);
    }
}
