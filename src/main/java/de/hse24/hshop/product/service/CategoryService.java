package de.hse24.hshop.product.service;

import de.hse24.hshop.product.model.Category;

import java.util.List;

public interface CategoryService {
    Category save(Category category);

    Category update(Category category);

    Category findById(String id);

    List<Category> findAllByLevel(int level);

    List<Category> findAll();

    Category findByNameAndLevel(String name, int level);

    void delete(String id);
}
