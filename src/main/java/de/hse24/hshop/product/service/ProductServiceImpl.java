package de.hse24.hshop.product.service;

import de.hse24.hshop.product.model.Product;
import de.hse24.hshop.product.repository.ProductRepository;
import de.hse24.hshop.common.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Implements service for manipulating products
 */
@Service
public class ProductServiceImpl implements ProductService {

    private static final String PRODUCT_NOT_FOUND = "Product not found";

    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Product save(Product product) {
        product.setId(UUID.randomUUID().toString());
        return productRepository.save(product);
    }

    @Override
    public Product update(Product product) {
        if (productRepository.findById(product.getId()).isEmpty()) {
            throw new EntityNotFoundException(PRODUCT_NOT_FOUND);
        }
        return productRepository.save(product);
    }

    @Override
    public Product findById(String id) {
        Optional<Product> product = productRepository.findById(id);
        if (product.isEmpty()) {
            throw new EntityNotFoundException(PRODUCT_NOT_FOUND);
        }
        return product.get();
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public void delete(String id) {
        productRepository.deleteById(id);
    }
}
