package de.hse24.hshop.product.service;

import de.hse24.hshop.product.model.Product;

import java.util.List;

public interface ProductService {
    Product save(Product category);

    Product update(Product category);

    Product findById(String id);

    List<Product> findAll();

    void delete(String id);
}
