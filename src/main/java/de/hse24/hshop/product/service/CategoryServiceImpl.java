package de.hse24.hshop.product.service;

import de.hse24.hshop.common.exception.DuplicatedEntityFoundException;
import de.hse24.hshop.common.exception.EntityNotFoundException;
import de.hse24.hshop.product.exception.HasSubCategoryException;
import de.hse24.hshop.product.exception.InvalidEntityException;
import de.hse24.hshop.product.model.Category;
import de.hse24.hshop.product.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Implements service for manipulating categories
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Category save(Category category) {
        category.setId(UUID.randomUUID().toString());
        validate(category, false);
        return trySave(category);
    }

    private Category trySave(Category category) {
        category.setLink(generateLink(category));
        return categoryRepository.save(category);
    }

    private void validate(Category category, boolean update) {
        if (!update && !categoryRepository.findByNameAndLevel(category.getName(), category.getLevel()).isEmpty()) {
            throw new DuplicatedEntityFoundException("A category with the same name and level already registered.");
        }
        if (category.getLevel() > 1 && category.getParentCategory() == null) {
            throw new InvalidEntityException("Parent category is missing.");
        }
        if (category.getLevel() > 1) {
            Optional<Category> parentCategory = categoryRepository.findById(category.getParentCategory().getId());
            if (parentCategory.isEmpty()) {
                throw new EntityNotFoundException("Parent category is not found.");
            }
            category.setParentCategory(parentCategory.get());
        } else {
            category.setParentCategory(null);
        }
    }

    String generateLink(Category category) {
        StringBuilder sb = new StringBuilder();
        sb.append(category.getName());
        Category parent = category.getParentCategory();
        while (parent != null) {
            sb.insert(0, parent.getName() + "/");
            parent = parent.getParentCategory();
        }
        return sb.toString();
    }

    @Override
    public Category update(Category category) {
        if (categoryRepository.findById(category.getId()).isEmpty()) {
            throw new EntityNotFoundException("Category not found");
        }
        validate(category, true);
        return trySave(category);
    }

    @Override
    public Category findById(String id) {
        Optional<Category> category = categoryRepository.findById(id);
        if (category.isEmpty()) {
            throw new EntityNotFoundException("Category not found");
        }
        return category.get();
    }

    @Override
    public List<Category> findAllByLevel(int level) {
        return categoryRepository.findAllByLevel(level);
    }

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category findByNameAndLevel(String name, int level) {
        List<Category> categories = categoryRepository.findByNameAndLevel(name, level);
        if (categories.isEmpty()) {
            throw new EntityNotFoundException("Category not found");
        }
        return categories.get(0);
    }

    @Override
    @Transactional
    public void delete(String id) {
        Optional<Category> category = categoryRepository.findById(id);
        if (category.isEmpty()) {
            return;
        }
        Category categoryEntity = category.get();
        int subCategories = categoryEntity.getSubCategories().size();
        if (subCategories > 0) {
            throw new HasSubCategoryException(String.format("Unable to delete! The category has %s sub categories.", subCategories));
        }
        if (categoryEntity.getParentCategory() != null) {
            Category toDeleteCategory = categoryEntity.getParentCategory().getSubCategories().stream()
                    .filter(item -> item.getId().equals(categoryEntity.getId()))
                    .findFirst().get();
            categoryEntity.getParentCategory().getSubCategories().remove(toDeleteCategory);
            categoryEntity.setParentCategory(null);
            categoryRepository.save(categoryEntity);
        }
        categoryRepository.deleteById(id);
    }
}
