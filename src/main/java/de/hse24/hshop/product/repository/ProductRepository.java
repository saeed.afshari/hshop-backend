package de.hse24.hshop.product.repository;

import de.hse24.hshop.product.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Implements repository pattern for Product entity
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, String> {
}
