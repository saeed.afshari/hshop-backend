package de.hse24.hshop.product.repository;

import de.hse24.hshop.product.model.Category;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Implements repository pattern for Category entity
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, String> {
    List<Category> findAllByLevel(int level);

    @EntityGraph(value = "GroupInfo.products", type = EntityGraph.EntityGraphType.LOAD)
    List<Category> findByNameAndLevel(String name, int level);
}
