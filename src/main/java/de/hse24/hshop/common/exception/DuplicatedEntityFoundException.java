package de.hse24.hshop.common.exception;

public class DuplicatedEntityFoundException extends RuntimeException {
    public DuplicatedEntityFoundException(String message) {
        super(message);
    }
}
