package de.hse24.hshop.common.exception;

public class FileNotFoundException extends RuntimeException {
    public FileNotFoundException(String message, Exception ex) {
        super(message, ex);
    }

    public FileNotFoundException(String message) {
        super(message);
    }
}
