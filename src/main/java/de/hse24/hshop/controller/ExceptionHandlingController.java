package de.hse24.hshop.controller;

import de.hse24.hshop.common.exception.DuplicatedEntityFoundException;
import de.hse24.hshop.common.exception.EntityNotFoundException;
import de.hse24.hshop.common.exception.FileNotFoundException;
import de.hse24.hshop.common.exception.FileStorageException;
import de.hse24.hshop.product.exception.HasSubCategoryException;
import de.hse24.hshop.product.exception.InvalidEntityException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Implements a global error handling controller in order to return understandable response to the clients
 */
@ControllerAdvice
@Slf4j
public class ExceptionHandlingController {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(EntityNotFoundException.class)
    public String handleNotFound(EntityNotFoundException exception) {
        return exception.getMessage();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(DataIntegrityViolationException.class)
    public String handleDataIntegrityViolationException(DataIntegrityViolationException exception) {
        log.error(exception.getMessage(), exception);
        return "Invalid input data. Please update";
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(DuplicatedEntityFoundException.class)
    public String handleDuplicatedEntityFoundException(DuplicatedEntityFoundException exception) {
        return exception.getMessage();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(InvalidEntityException.class)
    public String handleInvalidEntityException(InvalidEntityException exception) {
        return exception.getMessage();
    }

    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    @ExceptionHandler(HasSubCategoryException.class)
    public String handleInvalidEntityException(HasSubCategoryException exception) {
        return exception.getMessage();
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(FileNotFoundException.class)
    public String handleInvalidEntityException(FileNotFoundException exception) {
        return exception.getMessage();
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(FileStorageException.class)
    public String handleInvalidEntityException(FileStorageException exception) {
        return exception.getMessage();
    }

}
