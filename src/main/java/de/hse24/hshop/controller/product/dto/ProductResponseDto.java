package de.hse24.hshop.controller.product.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class ProductResponseDto {
    private String id;

    private String title;

    private String picture;

    private BigDecimal price;

    private String currency;

    private CategoryResponseDto category;
}
