package de.hse24.hshop.controller.product;

import de.hse24.hshop.controller.product.dto.ProductCreateOrUpdateDto;
import de.hse24.hshop.controller.product.dto.ProductResponseDto;
import de.hse24.hshop.controller.product.dto.mapper.ProductMapper;
import de.hse24.hshop.product.model.Category;
import de.hse24.hshop.product.model.Product;
import de.hse24.hshop.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * Implements CRUD end-points for product entity to let users manipulate products via REST
 */
@RestController
@RequestMapping(value = "v1/product", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductController {
    private final ProductService productService;
    private final ProductMapper productMapper;

    @Autowired
    public ProductController(ProductService productService,
                             ProductMapper productMapper) {
        this.productService = productService;
        this.productMapper = productMapper;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ProductResponseDto> getAll() {
        return productMapper.toProductResponseDtos(productService.findAll());
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ProductResponseDto get(@PathVariable String id) {
        return productMapper.toProductResponseDto(productService.findById(id));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String id) {
        productService.delete(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ProductResponseDto create(@RequestBody @Valid ProductCreateOrUpdateDto productCreateOrUpdateDto) {
        Product product = productMapper.toProduct(productCreateOrUpdateDto);
        setProductCategory(productCreateOrUpdateDto, product);
        return productMapper.toProductResponseDto(productService.save(product));
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ProductResponseDto update(@PathVariable String id,
                                     @RequestBody @Valid ProductCreateOrUpdateDto productCreateOrUpdateDto) {
        Product product = productMapper.toProduct(productCreateOrUpdateDto);
        product.setId(id);
        setProductCategory(productCreateOrUpdateDto, product);
        return productMapper.toProductResponseDto(productService.update(product));
    }

    private void setProductCategory(ProductCreateOrUpdateDto productCreateOrUpdateDto, Product product) {
        Category category = new Category();
        category.setId(productCreateOrUpdateDto.getCategoryId());
        product.setCategory(category);
    }
}
