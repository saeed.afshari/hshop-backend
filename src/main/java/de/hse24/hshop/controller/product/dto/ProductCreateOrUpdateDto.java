package de.hse24.hshop.controller.product.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class ProductCreateOrUpdateDto {
    private String id;

    @NotEmpty(message = "Name is mandatory")
    @NotNull(message = "Name is mandatory")
    private String title;

    @NotEmpty(message = "Picture is mandatory")
    @NotNull(message = "Picture is mandatory")
    private String picture;

    @Positive(message = "Price should be valid positive decimal number.")
    private BigDecimal price = BigDecimal.ZERO;

    @NotNull(message = "CategoryId is mandatory")
    @NotEmpty(message = "CategoryId is mandatory")
    private String categoryId;
}
