package de.hse24.hshop.controller.product.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class CategoryResponseDto {
    private String id;

    private String name;

    private String link;

    private int level;

    private List<CategoryResponseDto> subCategories;

    private List<ProductResponseDto> categoryProducts;

    private String parentCategoryId;

    private String parentCategoryName;
}
