package de.hse24.hshop.controller.product.dto.mapper;

import de.hse24.hshop.controller.product.dto.CategoryCreateOrUpdateDto;
import de.hse24.hshop.controller.product.dto.CategoryResponseDto;
import de.hse24.hshop.product.model.Category;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
@Component
public interface CategoryMapper {
    Category toCategory(CategoryCreateOrUpdateDto categoryCreateOrUpdateDto);

    @Mapping(source = "parentCategory.id", target = "parentCategoryId")
    @Mapping(source = "parentCategory.name", target = "parentCategoryName")
    CategoryResponseDto toCategoryResponseDto(Category category);

    List<CategoryResponseDto> toCategoryResponseDtos(List<Category> categories);
}
