package de.hse24.hshop.controller.product.dto.mapper;

import de.hse24.hshop.controller.product.dto.ProductCreateOrUpdateDto;
import de.hse24.hshop.controller.product.dto.ProductResponseDto;
import de.hse24.hshop.product.model.Product;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
@Component
public interface ProductMapper {
    Product toProduct(ProductCreateOrUpdateDto productCreateOrUpdateDto);

    ProductResponseDto toProductResponseDto(Product category);

    List<ProductResponseDto> toProductResponseDtos(List<Product> products);
}
