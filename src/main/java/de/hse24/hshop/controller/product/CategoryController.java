package de.hse24.hshop.controller.product;

import de.hse24.hshop.controller.product.dto.CategoryCreateOrUpdateDto;
import de.hse24.hshop.controller.product.dto.CategoryResponseDto;
import de.hse24.hshop.controller.product.dto.mapper.CategoryMapper;
import de.hse24.hshop.controller.product.dto.mapper.ProductMapper;
import de.hse24.hshop.product.model.Category;
import de.hse24.hshop.product.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * Implements CRUD end-points for category entity to let users manipulate product categories via REST
 */
@RestController
@RequestMapping(value = "v1/category", produces = MediaType.APPLICATION_JSON_VALUE)
public class CategoryController {
    private final CategoryService categoryService;
    private final CategoryMapper categoryMapper;
    private final ProductMapper productMapper;

    @Autowired
    public CategoryController(CategoryService categoryService,
                              CategoryMapper categoryMapper,
                              ProductMapper productMapper) {
        this.categoryService = categoryService;
        this.categoryMapper = categoryMapper;
        this.productMapper = productMapper;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<CategoryResponseDto> getAll() {
        return categoryMapper.toCategoryResponseDtos(categoryService.findAll());
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CategoryResponseDto get(@PathVariable String id) {
        return categoryMapper.toCategoryResponseDto(categoryService.findById(id));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String id) {
        categoryService.delete(id);
    }

    @GetMapping("/name/{name}/level/{level}")
    @ResponseStatus(HttpStatus.OK)
    public CategoryResponseDto getAllWithName(@PathVariable String name, @PathVariable int level) {
        return mapToCategoryResponseDto(categoryService.findByNameAndLevel(name, level));
    }

    private CategoryResponseDto mapToCategoryResponseDto(Category category) {
        CategoryResponseDto categoryResponseDto = categoryMapper.toCategoryResponseDto(category);
        if (categoryResponseDto != null) {
            categoryResponseDto.setCategoryProducts(productMapper.toProductResponseDtos(category.getProducts()));
        }
        return categoryResponseDto;
    }

    @GetMapping("/level/{level}")
    @ResponseStatus(HttpStatus.OK)
    public List<CategoryResponseDto> getAllWithLevel(@PathVariable int level) {
        return categoryMapper.toCategoryResponseDtos(categoryService.findAllByLevel(level));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CategoryResponseDto create(@RequestBody @Valid CategoryCreateOrUpdateDto categoryCreateOrUpdateDto) {
        Category category = categoryMapper.toCategory(categoryCreateOrUpdateDto);
        category.setParentCategory(Category.from(categoryCreateOrUpdateDto.getParentCategoryId()));
        return categoryMapper.toCategoryResponseDto(categoryService.save(category));
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CategoryResponseDto update(@PathVariable String id,
                                      @RequestBody @Valid CategoryCreateOrUpdateDto categoryCreateOrUpdateDto) {
        Category category = categoryMapper.toCategory(categoryCreateOrUpdateDto);
        category.setId(id);
        category.setParentCategory(Category.from(categoryCreateOrUpdateDto.getParentCategoryId()));
        return categoryMapper.toCategoryResponseDto(categoryService.update(category));
    }
}
