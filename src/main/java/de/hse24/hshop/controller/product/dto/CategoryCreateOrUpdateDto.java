package de.hse24.hshop.controller.product.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@NoArgsConstructor
public class CategoryCreateOrUpdateDto {
    private String id;
    @NotNull(message = "Name is mandatory")
    private String name;

    @NotNull
    @Positive(message = "Level should be valid positive integer number.")
    private int level;

    private String parentCategoryId;
}
