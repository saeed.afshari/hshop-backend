-- liquibase formatted sql

-- changeset saeedafshari:create-category-table

CREATE TABLE category
(
    id                  VARCHAR(50)               PRIMARY KEY,
    name                VARCHAR(255)              NOT NULL,
    level               INT                       DEFAULT 1 NOT NULL,
    link                VARCHAR(1000)             NOT NULL,
    parent_id           VARCHAR(50)               NULL,
    created             datetime                  NOT NULL,
    updated             datetime                  NOT NULL,
    deleted             datetime                  NULL
);
