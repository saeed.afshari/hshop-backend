-- liquibase formatted sql

-- changeset saeedafshari:create-product-table

CREATE TABLE product
(
    id          VARCHAR(50) PRIMARY KEY,
    title       VARCHAR(255)              NOT NULL,
    picture     VARCHAR(1000)             NOT NULL,
    price       DECIMAL(15, 2)            NOT NULL,
    currency    VARCHAR(10) DEFAULT 'EUR' NOT NULL,
    category_id VARCHAR(50)               NOT NULL,
    created     datetime                  NOT NULL,
    updated     datetime                  NOT NULL,
    deleted     datetime                  NULL
);

ALTER TABLE product
    ADD FOREIGN KEY (category_id) REFERENCES category (id);
